package com.kshrd.homework005.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.kshrd.homework005.entity.BookEntity;
import com.kshrd.homework005.entity.CategoryEntity;

import java.util.List;

@Dao
public interface CategoryDao {

    @Query("select * from tb_category")
    List<CategoryEntity> findAllCategory();

    @Insert()
    void insertCategory(CategoryEntity categoryEntity);
}

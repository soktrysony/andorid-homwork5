package com.kshrd.homework005.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.kshrd.homework005.entity.BookEntity;

import java.util.List;

@Dao
public interface BookDao {
    @Query("SELECT * FROM tb_book")
    List<BookEntity> findAllBook();

    @Query("SELECT * FROM tb_book")
    LiveData<List<BookEntity>> findAllBooks();

    @Query("SELECT * FROM tb_book WHERE id = :id")
    List<BookEntity> findOne(int id);

    @Insert()
    void insertBook(BookEntity book);

    @Update()
    void updateBook(BookEntity book);

    @Delete()
    void deleteBook(BookEntity book);
}

package com.kshrd.homework005.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.kshrd.homework005.R;
import com.kshrd.homework005.entity.BookEntity;
import com.kshrd.homework005.entity.CategoryEntity;
import com.kshrd.homework005.room.AppDatabase;

import java.util.List;

public class CustomDialog extends AppCompatDialogFragment {
    private EditText title, size, price;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog, null);

        Spinner mySpinner = view.findViewById(R.id.category_Spinner);
        List<CategoryEntity> allCategory = AppDatabase.getDatabase(getContext()).categoryDao().findAllCategory();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.category));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mySpinner.setAdapter(adapter);

        builder.setView(view)
                .setTitle("Insert Book")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        addNewBook();
                    }
                });
        title = view.findViewById(R.id.book_title);
        size = view.findViewById(R.id.book_size);
        price = view.findViewById(R.id.book_price);


        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                Toast.makeText(getContext(),"dismissed",Toast.LENGTH_LONG).show();
            }
        });
        return builder.create();
    }


    private void addNewBook() {
        String title = this.title.getText().toString();
        String size = this.size.getText().toString();
        String price = this.price.getText().toString();
//        double size = Double.parseDouble(editTextSize.getText().toString());
//        double price = Double.parseDouble(editTextPrice.getText().toString());
        BookEntity book = new BookEntity();
        book.setTitle(title);
        book.setSize(size);
        book.setPrice(price);
        AppDatabase.getDatabase(getContext()).bookDao().insertBook(book);
    }
}

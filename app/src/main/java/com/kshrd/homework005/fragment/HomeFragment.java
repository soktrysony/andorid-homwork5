package com.kshrd.homework005.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kshrd.homework005.MyListAdapter;
import com.kshrd.homework005.R;
import com.kshrd.homework005.entity.BookEntity;
import com.kshrd.homework005.room.AppDatabase;

import java.util.List;

public class HomeFragment extends Fragment {
    RecyclerView recyclerView;
    List<BookEntity> bookEntityList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home,container,false);

        bookEntityList = AppDatabase.getDatabase(getContext()).bookDao().findAllBook();
        recyclerView = view.findViewById(R.id.myRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        recyclerView.setAdapter(new MyListAdapter(bookEntityList));
        return view;
    }
}

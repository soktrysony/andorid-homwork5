package com.kshrd.homework005.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tb_category")
public class CategoryEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String category;

    public CategoryEntity() {
    }

    @Override
    public String toString() {
        return "CategoryEntity{" +
                "id=" + id +
                ", category='" + category + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}

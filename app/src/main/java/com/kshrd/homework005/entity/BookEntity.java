package com.kshrd.homework005.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tb_book")
public class BookEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String size, price;
    private String title, image, category;

    public BookEntity() {
    }

    @Override
    public String toString() {
        return "BookEntity{" +
                "id=" + id +
                ", size='" + size + '\'' +
                ", price='" + price + '\'' +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", category='" + category + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}

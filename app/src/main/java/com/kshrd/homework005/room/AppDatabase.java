package com.kshrd.homework005.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.kshrd.homework005.dao.BookDao;
import com.kshrd.homework005.dao.CategoryDao;
import com.kshrd.homework005.entity.BookEntity;
import com.kshrd.homework005.entity.CategoryEntity;

@Database(entities = {BookEntity.class, CategoryEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract BookDao bookDao();
    public abstract CategoryDao categoryDao();
    public static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "db_books")
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

}

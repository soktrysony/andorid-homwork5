package com.kshrd.homework005;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kshrd.homework005.entity.BookEntity;

import java.util.List;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.MyViewHolder> {

    private final List<BookEntity> bookEntityList;

    public MyListAdapter(List<BookEntity> bookEntityList) {
        this.bookEntityList = bookEntityList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listView = layoutInflater.inflate(R.layout.layout_item, parent, false);

        return new MyViewHolder(listView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        BookEntity bookEntity = bookEntityList.get(position);
        Log.d("TEST", "onBindViewHolder: "+position);
        holder.title.setText(bookEntity.getTitle());
        holder.size.setText(bookEntity.getSize());
        holder.price.setText(bookEntity.getPrice());
    }

    @Override
    public int getItemCount() {
        return bookEntityList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView title, size, price;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.txt_title);
            size = itemView.findViewById(R.id.txt_size);
            price = itemView.findViewById(R.id.txt_price);
        }
    }
}


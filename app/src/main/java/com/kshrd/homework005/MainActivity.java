package com.kshrd.homework005;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.kshrd.homework005.dialog.CustomDialog;
import com.kshrd.homework005.entity.BookEntity;
import com.kshrd.homework005.fragment.DashboardFragment;
import com.kshrd.homework005.fragment.HomeFragment;
import com.kshrd.homework005.fragment.NotificationsFragment;
import com.kshrd.homework005.room.AppDatabase;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);
        if (savedInstanceState == null) {
            bottomNavigationView.setSelectedItemId(R.id.page_1);
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_placeholder, new HomeFragment())
                .commit();
        findAll();

    }


    public BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @SuppressLint("NonConstantResourceId")
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment;

            switch (item.getItemId()) {
                case R.id.page_2:
                    getSupportActionBar().setTitle("Dashboard");
                    selectedFragment = new DashboardFragment();
                    break;
                case R.id.page_3:
                    getSupportActionBar().setTitle("Notifications");
                    selectedFragment = new NotificationsFragment();
                    break;
                case R.id.page_1:
                default:
                    getSupportActionBar().setTitle("Home");
                    selectedFragment = new HomeFragment();
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_placeholder, selectedFragment).commit();

            return true;
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.option_search:
                //go to search
                Toast.makeText(this, "Searching", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.option_add:
                //go to search
                Toast.makeText(this, "Adding", Toast.LENGTH_SHORT).show();
                openDialog();
                findAll();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openDialog() {
        CustomDialog dialog = new CustomDialog();
        dialog.show(getSupportFragmentManager(), "custom dialog");
    }

    public void findAll() {
        List<BookEntity> allBooks = AppDatabase.getDatabase(this).bookDao().findAllBook();
        Log.d("TAG", allBooks.toString());
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_placeholder, new HomeFragment())
                .addToBackStack(null)
                .commit();
    }

    public void insert() {
        BookEntity book = new BookEntity();
        book.setTitle("java");

        AppDatabase.getDatabase(this).bookDao().insertBook(book);
    }

}